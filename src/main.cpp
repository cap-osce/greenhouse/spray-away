#include <Arduino.h>
#include "Servo.h"
#include "IOTGateway.h"
#include "StandardConfig.h"
#include "U8x8lib.h"
#include <ArduinoJson.h>

Servo servo;
IOTGateway iotGateway(&capgeminiHackathon);

U8X8_SSD1306_128X64_NONAME_SW_I2C display(4, 5, U8X8_PIN_NONE);

int msgReceived = 0;
String payload;
String rcvdPayload;
int latest;
long lastSprayTime = 0;
DeserializationError err(DeserializationError::Ok);

void callBackHandler (char *topicName, int payloadLen, char *payLoad) {
  char rcvdPayloadChar[512];

  strncpy(rcvdPayloadChar, payLoad, payloadLen);
  rcvdPayloadChar[payloadLen] = 0;
  rcvdPayload = rcvdPayloadChar;

  StaticJsonDocument<512> message;
  err = deserializeJson(message, rcvdPayload);

  if (err) {
    Serial.print(F("deserializeJson() failed with code "));
    Serial.println(err.c_str());
    msgReceived = 0;
  } else {
    latest = message["value"];
    msgReceived = 1;
  }
}
 
boolean shouldSpray() {
  if(msgReceived == 1) {
    msgReceived = 0;
    Serial.print("Received: ");
    Serial.println(latest);
    if (latest > 0 && (millis() - lastSprayTime) >= 5000) {
      lastSprayTime = millis();
      return true;
    }
  } else if (err) {
    display.drawString(0,1,"DES!!");
    display.refreshDisplay();
  }
  return false;
}

void setup() {
  Serial.begin(115200);
  display.begin();
  display.clear();
  
  display.setFont(u8x8_font_amstrad_cpc_extended_f);
  display.drawString(0,0,"Start...");
  display.refreshDisplay();

  iotGateway.initialise();
  display.clear();
  display.drawString(0,0,"Subscribe");
  display.refreshDisplay();

  delay(2500);
  iotGateway.subscribe("command/water", callBackHandler);
  display.clear();
  display.drawString(0,0,"Waiting...");
  display.refreshDisplay();
  display.setFont(u8x8_font_courB18_2x3_f);
  servo.attach(25, -1, 0, 180, 800, 2200);
}

long lastTime = 0;

void loop() {
  if (shouldSpray()) {
    display.drawString(0,1,"SPRAY");
    display.refreshDisplay();
    lastTime = millis();
    servo.write(0);
    delay(750);
    servo.write(180);
    delay(750);
    display.drawString(0,1,"     ");
    display.refreshDisplay();
  }
  delay(1000);
}